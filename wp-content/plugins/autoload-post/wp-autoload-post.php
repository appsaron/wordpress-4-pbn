<?php
/*
Plugin Name: WP Autoload Posts
Description: Simple plugin that load post. Visit "WP Autoload Posts" setting in the dasboard meny to load posts.
Version:     1.0.0
Author:      Shovhan
Author URI:  https://facebook.com/shovhan.stas
*/

if (!defined('ABSPATH')) exit;

require_once __DIR__ . '/session.php';
remove_action( 'shutdown', 'wp_ob_end_flush_all', 1 );

function tpl_post_autoload_run($controller = '', $action = '') {
    if( WP_DEBUG && WP_DEBUG_DISPLAY && (defined('DOING_AJAX') && DOING_AJAX) ){
        @ ini_set( 'display_errors', 1 );
        error_reporting(-1); // reports all errors
        ini_set("display_errors", "1"); // shows all errors
        ini_set("log_errors", 1);
        // ini_set("error_log", "/tmp/php-error.log");
    }

    defined('TPLVIEWS') || define('TPLVIEWS', __DIR__ . '/src/views');

    require_once __DIR__ . '/vendor/autoload.php';
    require_once __DIR__ . '/container.php';

    /* @var \Pimple\Container $container */
    if(!empty($controller) && !empty($action)) {
        try {
            $container[$controller]->{$action}();
        } catch (\AutoloadPost\Action\ValidationException $e) {
            print_r(json_encode($e->getErrors()));
            exit;
        }
    }

    return $container;
}

/*
 * Добавляет пункт в меню админки
 */
add_action('admin_menu','tpl_add_menu_button');

function tpl_add_menu_button() {
    $page = add_menu_page(
        __('Loader', 'tpl_form'),
        __('Loader', 'tpl_form'),
        'manage_options',
        'tpl_settings',
        'tpl_form',
        'dashicons-portfolio'
    );

    /*
     * Грузит файлы стилей и скриптов для страницы настроек плагина
     */
    add_action('admin_print_styles-'. $page, 'tpl_include_css');
    add_action('admin_print_scripts-'. $page, 'tpl_include_js');
}

function tpl_include_css() {
    wp_enqueue_style(
        'tpl-style',
        plugins_url('/src/public/style.css?v=52', __FILE__)
    );
}

function tpl_include_js() {
    wp_enqueue_script(
        'tpl-script',
        plugins_url( '/src/public/script.js?v=93', __FILE__ ),
        array( 'jquery' )
    );
    wp_enqueue_editor();
    wp_localize_script(
        'tpl-script', 'paths', ['ajaxPath' => home_url() . '/wp-admin/admin-ajax.php'] );
}

/*
 * Код страницы, которая откроется при клике на пункт в меню админки
 */
function tpl_form() {
    require_once(TPLVIEWS . '/plugin-settings-form.php');
}

/*
 * Авторизация в Google Drive
 */ 
add_action('load-toplevel_page_tpl_settings', function () {
    if(!isset($_POST['access_token'])) {
        tpl_post_autoload_run(\AutoloadPost\Action\GoogleDrive\Auth\RequestAction::class, 'handle');
    } else {
        tpl_post_autoload_run(\AutoloadPost\Action\GoogleDrive\Auth\ConfirmAction::class, 'handle');
    }
});

/*
 * Обрабатывает AJAX запросы
 */
if(wp_doing_ajax() || defined('DOING_AJAX')) {
    add_action('wp_ajax_parse_drive', 'tpl_parse_drive');
    add_action('wp_ajax_save_post', 'tpl_save_post');
    add_action('wp_ajax_files_list', 'tpl_files_list');


    function tpl_parse_drive() {
        tpl_post_autoload_run(\AutoloadPost\Action\File\ParseAction::class, 'handle');
    }

    function tpl_save_post() {
        tpl_post_autoload_run(\AutoloadPost\Action\Post\SaveAction::class, 'handle');
    }

    function tpl_files_list() {
        tpl_post_autoload_run(\AutoloadPost\Action\File\ListAction::class, 'handle');
    }
}