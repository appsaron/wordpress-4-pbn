<?php

/** @var \Pimple\Container container */
$container['config'] = [
    'drive' => [
        'authUrl' => 'http://shovhan.com/index.php'
    ],
    'imageDownload' => [
        'imageSize' => 'medium'
    ]
];