<div class="tpl-global-wrap">
    <div class="tpl-wrap">
        <div class="tpl-block tpl-block__active">
            <p class="tpl-block--title tpl-block--title__shadow">Specify the source of the download</p>
            <div class="tpl-block--body tpl-block--body__blue">
                <form class="tpl_ajax_form" id="parse_drive" data-success-function="driveParsed">
                    <div class="tpl-flex">
                        <div class="tpl-white-block">
                            <label class="tpl-label" for="drive-folder-input">Google Drive</label>
                            <textarea name="drive-folder" class="tpl-input" id="drive-folder-input" placeholder="Insert here a valid links to Google Drive" rows="4">https://drive.google.com/drive/folders/1n74ko9eWq6SNJrjLA8I-7Y5hKu3bdzXc</textarea>
                        </div>

                        <div class="tpl-white-block">
                            <label class="tpl-label" for="drive-file-input">Google Docs (Multiple)</label>
                            <textarea name="drive-files" id="drive-file-input" rows="4" class="tpl-input" placeholder="Insert here a list with valid links to Google Docs"></textarea>
                        </div>
                    </div>
                    <div class="tpl-flex">
                        <button type="submit" class="tpl-button tpl-submit-button">
                            <img src="<?php echo plugins_url('src/public/preloader.svg', dirname(TPLVIEWS)); ?>" class="tpl-hidden" width="25" height="25">
                            <span>Download</span>
                        </button>
                        <div class="tpl-progress-bar"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="tpl-wrap tpl-posts">
    </div>

    <button type="button" class="tpl-button tpl-post-save-button tpl-hidden" id="tpl-button-save-all">Save All</button>
</div>

<template id="tpl-post-template">
    <div class="tpl-block">
        <div class="tpl-block--title">
            <span class="tpl-block--title-text"></span>
            <input class="tpl-input tpl-block--title-input">
            <div class="tpl-save-buttons">
                <button type="button" class="tpl-button tpl-post-copy-button">
                    <img width="25" height="25" src="<?php echo plugins_url('src/public/copy.svg', dirname(TPLVIEWS)); ?>">
                    <span>Copy Settings</span>
                </button>
                <!-- <button type="button" class="tpl-button tpl-post-reload-button">Reload</button> -->
                <button type="button" class="tpl-button tpl-post-save-button tpl-post-save-button">
                    <img width="25" height="25" id="tpl-img-load" src="<?php echo plugins_url('src/public/preloader.svg', dirname(TPLVIEWS)); ?>" class="tpl-hidden">
                    <img id="tpl-img-disc" src="<?php echo plugins_url('src/public/floppy-disk.svg', dirname(TPLVIEWS)); ?>" width="25" height="25">
                    <span>Save</span>
                </button>
            </div>
        </div>

        <div class="tpl-block--body" style="display: none;">
            <div class="tpl-flex">
                <div class="tpl-wrap tpl-main-blocks">
                    <div class="tpl-block tpl-block__active">
                        <p class="tpl-block--title  tpl-block--title__shadow">Add selector</p>
                        <div class="tpl-block--body tpl-block--body__blue">
                            <div class="selector-list"></div>
                            <button type="button" class="tpl-add-selector">+</button>
                        </div>
                    </div>
                </div>

                <div class="tpl-wrap tpl-main-blocks">
                    <div class="tpl-block tpl-block__active tpl-hidden">
                        <p class="tpl-block--title  tpl-block--title__shadow">Choose post element for selector</p>
                        <div class="tpl-block--body tpl-block--body__blue">
                            <div class="post-entities"></div>
                        </div>
                    </div>
                </div>

                <div class="tpl-wrap tpl-main-blocks">
                    <div class="tpl-block tpl-block__active tpl-hidden">
                        <p class="tpl-block--title  tpl-block--title__shadow">Preview</p>
                        <div class="tpl-block--body tpl-block--body__blue tpl-previews">

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="tpl-alerts-block"></div>
    </div>
</template>

<template id="tpl-preview-template">
    <label>
        <span class="tpl-preview--open-tag"></span>
        <p class="error tpl-hidden"></p>
        <textarea rows="4" class="tpl-input"></textarea>
        <span class="tpl-preview--close-tag"></span>
    </label>
</template>