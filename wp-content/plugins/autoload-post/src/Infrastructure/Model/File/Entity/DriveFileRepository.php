<?php

namespace AutoloadPost\Infrastructure\Model\File\Entity;

use AutoloadPost\Model\File\Entity\FileRepository;
use AutoloadPost\Model\GoogleDrive\Entity\TokenRepository;
use AutoloadPost\Model\File\Entity\File;
use AutoloadPost\Model\GoogleDrive\Service\Downloader\Downloader;
use AutoloadPost\Model\GoogleDrive\Service\Downloader\AccessToken;
use AutoloadPost\Model\GoogleDrive\Service\Downloader\HtmlCleaner;

class DriveFileRepository implements FileRepository
{
	private $files = [];

	public function getAllByUrls(array $fileUrls, array $folderUrls)
	{
	    
		$token = (new TokenRepository())->get();
		$downloader = new Downloader(new AccessToken($token->token()));

		$htmlCleaner = new HtmlCleaner(
			new \AutoloadPost\Model\Service\HtmlParser\PhpQueryParser()
		);

// 		foreach($folderUrls as $url) {
// 			$filesFromFolder = $downloader->fileListFromFolder($url);
// 			$fileUrls = array_merge($fileUrls, $filesFromFolder);
// 		}
		 

		foreach($fileUrls as $url) {
			$file =	$downloader->download($url);
			$file = $htmlCleaner->clean($file);
			$this->files[] = new File($file->name, $file->content, $file->srcList);
		}

		return $this->files;
	}

    public function getFileNamesFromFolder(array $folderUrls)
    {
        $fileUrls = [];

        $token = (new TokenRepository())->get();
        $downloader = new Downloader(new AccessToken($token->token()));

        foreach($folderUrls as $url) {
            $filesFromFolder = $downloader->fileListFromFolder($url);
            $fileUrls = array_merge($fileUrls, $filesFromFolder);
        }

        return $fileUrls;
	}
}