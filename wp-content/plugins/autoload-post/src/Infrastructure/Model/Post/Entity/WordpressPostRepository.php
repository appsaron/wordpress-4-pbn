<?php

namespace AutoloadPost\Infrastructure\Model\Post\Entity;

use AutoloadPost\Model\Post\Entity\Post;
use AutoloadPost\Model\Post\Entity\PostRepository;

class WordpressPostRepository implements PostRepository
{
	public function save(Post $post)
    {
        $fields = [
            'post_title'     => $post->title(),
            'post_content'   => $post->content(),
            'post_date'      => $post->date()->format('Y-m-d H:i:s'),
            'post_author'    => $post->author(),
            'post_type'      => $post->type(),
            'post_status'    => $post->status()
        ];

        $id = wp_insert_post(
            array_filter(
                $fields
            )
        );
        $post->setId($id);
    }

    /**
     * @param Post $post
     * @throws \Exception
     *
     * @return void
     */
    public function saveChangedContent(Post $post)
    {
        $id = wp_update_post([
            'ID' => $post->id(),
            'post_content' => $post->content()
        ]);

        if($id == 0) {
            throw new \Exception("Can't update post");
        }
    }
}