<?php

namespace AutoloadPost\Infrastructure\Model\Post\Entity;

use AutoloadPost\Model\Post\Entity\Category;
use AutoloadPost\Model\Post\Entity\CategoryRepository;

class WordpressCategoryRepository implements CategoryRepository
{
    public function save(Category $category, int $postId)
    {
        wp_set_post_terms($postId, wp_create_category($category->value()), 'category');
    }
}