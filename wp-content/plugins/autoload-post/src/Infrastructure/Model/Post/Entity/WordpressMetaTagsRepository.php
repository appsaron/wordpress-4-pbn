<?php

namespace AutoloadPost\Infrastructure\Model\Post\Entity;

use AutoloadPost\Model\Post\Entity\MetaTag;
use AutoloadPost\Model\Post\Entity\MetaTagsRepository;

class WordpressMetaTagsRepository implements MetaTagsRepository
{
    public function save(MetaTag $metaTag, int $postId)
    {
        add_post_meta($postId, $metaTag->key(), $metaTag->value());
    }
}