<?php

namespace AutoloadPost\Model\Post\Entity;

class MetaTag
{
	private $post;
	private $key;
	private $value;

    /**
     * MetaTag constructor.
     * @param Post $post
     * @param string $key
     * @param string $value
     *
     * @return void
     */
    public function __construct(Post $post, string $key, string $value)
	{
		$this->post = $post;
		$this->key = $key;
		$this->value = $value;
	}

    public function postId()
    {
        return $this->post->id();
	}

    /**
     * @return string
     */
    public function key()
    {
        return $this->key;
    }

    /**
     * @return string
     */
    public function value()
    {
        return $this->value;
    }


}