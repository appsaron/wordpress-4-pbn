<?php

namespace AutoloadPost\Model\Post\Entity;

interface CategoryRepository
{
    /**
     * @param Category $category
     * @param int $postId
     *
     * @return mixed
     */
    public function save(Category $category, int $postId);
}