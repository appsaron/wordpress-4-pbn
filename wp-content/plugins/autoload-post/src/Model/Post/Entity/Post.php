<?php

namespace AutoloadPost\Model\Post\Entity;

class Post
{
    private $id;
	private $content;
	private $date;
	private $title;
	private $type;
	private $status;
	private $author = '';

    /**
     * Post constructor.
     * @param string $title
     * @param string $content
     * @param \DateTimeImmutable $date
     * @param $type
     * @param $status
     */
    public function __construct(
		string $title,
		string $content,
		\DateTimeImmutable $date,
		$type,
		$status
	) {
		$this->content = $content;
		$this->date = $date;
		$this->title = $title;
		$this->type = $type;
		$this->status = $status;
		$this->author = $this->author();
	}

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
	}

    /**
     * @return int
     */
    public function author()
	{
		if(empty($this->author)) {
			$this->author = get_current_user_id() == 0 ? 1 : get_current_user_id();
		}

		return $this->author;
	}

    /**
     * @param string $content
     */
    public function changeContent(string $content)
    {
        $this->content = $content;
	}

    /**
     * @return string
     */
    public function content()
    {
        return $this->content;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function date()
    {
        return $this->date;
    }

    /**
     * @return int
     */
    public function id()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function title()
    {
        return $this->title;
    }

    /**
     * @return mixed
     */
    public function type()
    {
        return $this->type;
    }

    /**
     * @return mixed
     */
    public function status()
    {
        return $this->status;
    }


}