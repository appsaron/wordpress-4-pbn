<?php

namespace AutoloadPost\Model\Post\Entity;

interface PostRepository
{
    public function save(Post $post);
    public function saveChangedContent(Post $post);
}