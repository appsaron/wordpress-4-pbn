<?php

namespace AutoloadPost\Model\Post\Entity;

interface MetaTagsRepository
{
    public function save(MetaTag $metaTag, int $postId);
}