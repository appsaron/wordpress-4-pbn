<?php

namespace AutoloadPost\Model\Post\Entity;

class Category
{
    private $value;
    private $postId;

    public function __construct(string $value, string $postId)
    {
        $this->value = $value;
        $this->postId = $postId;
    }

    /**
     * @return string
     */
    public function value(): string
    {
        return $this->value;
    }

    /**
     * @return string
     */
    public function postId(): string
    {
        return $this->postId;
    }
}