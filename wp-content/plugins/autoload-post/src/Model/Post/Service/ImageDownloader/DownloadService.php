<?php

namespace AutoloadPost\Model\Post\Service\ImageDownloader;

class DownloadService
{
    /**
     * @param SrcList $srcList
     * @param ImageDownloaderInterface $imageService
     *
     * @return SrcList
     * @throws \Exception
     */
    public function downloadImages(SrcList $srcList, ImageDownloaderInterface $imageService)
    {
        $newSrcList = new SrcList();

        foreach ($srcList->srcList() as $src) {
            $imageService->setPostId($srcList->postId());
            $newSrc = $imageService->download($src);
            $newSrcList->addSrc($newSrc);
        }

        return $newSrcList;
    }

    /**
     * @param string $postContent
     * @param SrcList $srcList
     * @return mixed|string
     */
    public function changeImgTag(string $postContent, SrcList $srcList)
    {
        foreach($srcList->srcList() as $src) {
            $postContent = $this->str_replace_first('<img>', $src, $postContent);
        }
        return $postContent;
    }

    private function str_replace_first($from, $to, $content)
    {
        $from = '/'.preg_quote($from, '/').'/';
        return preg_replace($from, $to, $content, 1);
    }
}