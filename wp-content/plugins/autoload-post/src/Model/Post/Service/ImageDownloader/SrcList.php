<?php

namespace AutoloadPost\Model\Post\Service\ImageDownloader;

class SrcList
{
	private $srcList;
    private $postId;

	public function __construct(int $postId = 0, array $srcList = [])
	{
	    $this->postId = $postId;
		$this->srcList = $srcList;
	}

    /**
     * @return array
     */
    public function srcList()
	{
		return $this->srcList;
	}

    /**
     * @return int
     * @throws \Exception
     */
    public function postId()
    {
        if($this->postId == 0) {
            throw new \Exception('Something went wrong');
        }

        return $this->postId;
    }

    /**
     * @param $src
     * @return void
     */
    public function addSrc($src)
	{
		$this->srcList[] = $src;
	}
}