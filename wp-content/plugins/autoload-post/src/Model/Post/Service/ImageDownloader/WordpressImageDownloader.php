<?php

namespace AutoloadPost\Model\Post\Service\ImageDownloader;

class WordpressImageDownloader implements ImageDownloaderInterface
{
    private $postId;
    private $alt = '';
    private $config = [];

    public function __construct(array $config = [])
    {
        $this->config = $config;
    }

    public function download(string $url)
	{
        $image_tmp = download_url($url);

        if( is_wp_error( $image_tmp ) ){
            throw new \Exception($image_tmp->get_error_message());
        } else {
            $image_size = filesize($image_tmp);
            $image_name = basename($url) . '.jpg';

            $file = array(
                'name' => $image_name,
                'type' => 'image/jpg',
                'tmp_name' => $image_tmp,
                'error' => 0,
                'size' => $image_size
            );

            $id = media_handle_sideload( $file, $this->postId, $this->alt);
            if(is_wp_error($id)) {
                throw new \Exception($id->get_error_message());
            }

            return wp_get_attachment_image($id, $this->config['imageSize']);
        }
	}

    /**
     * @param string $postId
     */
    public function setPostId(string $postId)
    {
        $this->postId = $postId;
	}

    /**
     * @param string $alt
     */
    public function setAlt(string $alt)
    {
        $this->alt = $alt;
	}
}