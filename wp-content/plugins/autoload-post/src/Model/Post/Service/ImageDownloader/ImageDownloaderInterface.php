<?php

namespace AutoloadPost\Model\Post\Service\ImageDownloader;

interface ImageDownloaderInterface
{
    public function __construct(array $config);
	public function download(string $url);
	public function setPostId(string $postId);
}