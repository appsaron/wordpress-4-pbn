<?php

namespace AutoloadPost\Model\Post\Service\ImageDownloader;

class Images
{
	public function downloadImages(
		SrcList $srcList, 
		ImageDownloaderInterface $downloader
	) {
		$newSrcList = new SrcList();
		foreach($srcList->srcList() as $key => $src) {
			$downloadedImageSrc = $downloader->download($src);
			$newSrcList->addSrc($downloadedImageSrc);
		}
		return $newSrcList;
	}

	public function changeImgTag(string $html, SrcList $srcList)
	{
		$tags = [];
		foreach($srcList->srcList() as $src) {
			$tags[] = '<img src="' . $src . '">';
		}
		foreach($tags as $tag) {
			$html = str_replace('<img>', $tag, $html);
		}
		return $html;
	}
}