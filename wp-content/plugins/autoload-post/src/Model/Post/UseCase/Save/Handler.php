<?php

namespace AutoloadPost\Model\Post\UseCase\Save;

use AutoloadPost\Model\Post\Entity\PostRepository;
use AutoloadPost\Model\Post\Entity\CategoryRepository;
use AutoloadPost\Model\Post\Entity\MetaTagsRepository;
use AutoloadPost\Model\Post\Entity\Post;
use AutoloadPost\Model\Post\Entity\Category;

use AutoloadPost\Model\Post\Service\ImageDownloader\DownloadService;
use AutoloadPost\Model\Post\Service\ImageDownloader\SrcList;
use AutoloadPost\Model\Post\Entity\MetaTag;
use AutoloadPost\Model\Post\Service\ImageDownloader\ImageDownloaderInterface;

class Handler
{
    /**
     * @var PostRepository
     */
    private $posts;

    /**
     * @var CategoryRepository
     */
    private $categories;

    /**
     * @var MetaTagsRepository
     */
    private $metaTags;

    /**
     * @var DownloadService
     */
    private $imageService;

    /**
     * @var array
     */
    private $errors = [];

    /**
     * @var ImageDownloaderInterface
     */
    private $imageDownloader;

    /**
     * Save post handler constructor.
     * @param PostRepository $posts
     * @param CategoryRepository $categories
     * @param MetaTagsRepository $metaTags
     * @param DownloadService $imageService
     *
     * @param ImageDownloaderInterface $imageDownloader
     */
    public function __construct(
        PostRepository $posts,
        CategoryRepository $categories,
        MetaTagsRepository $metaTags,
        DownloadService $imageService,
        ImageDownloaderInterface $imageDownloader
    ) {
        $this->posts = $posts;
        $this->categories = $categories;
        $this->metaTags = $metaTags;
        $this->imageService = $imageService;
        $this->imageDownloader = $imageDownloader;
    }

    /**
     * @param $command
     * @throws \Exception
     */
    public function handle($command)
    {
        foreach ($command->posts as $key => $post) {
            if(!isset($this->errors[$key])) {
                try {
                    $this->savePost($post);
                } catch (\Exception $e) {
                    $this->errors[$key][] = $e->getMessage();
                }
            }
        }
    }

    /**
     * @param array $errors
     * @return void
     */
    public function setErrors(array $errors)
    {
        $this->errors = $errors;
    }

    public function errors()
    {
        return $this->errors;
    }

    private function savePost(array $commandPost)
    {
        $commandTags = $commandPost['tags'];

        $dateTimeImmutable = new \DateTimeImmutable();

        $content = '';
        $strings = explode("\n", $commandTags['content']);
        foreach($strings as $string) {
            $content .= '<p>' . $string . '</p>';
        }

        $post = new Post(
            $commandTags['title'],
            $content,
            $dateTimeImmutable->setTimestamp($commandTags['date']),
            $commandTags['type'],
            $commandTags['status']
        );

        $this->posts->save($post);

        $srcList = new SrcList($post->id(), $commandPost['srcList']);
        $srcList = $this->imageService->downloadImages($srcList, $this->imageDownloader);
        $content = $this->imageService->changeImgTag($post->content(), $srcList);

        $post->changeContent($content);

        $this->posts->saveChangedContent($post);

        $category = new Category($commandTags['category'], $post->id());
        $this->categories->save($category, $post->id());

        foreach($commandTags['metaTags'] as $metaName => $metaValue) {
            $this->metaTags->save(new MetaTag($post, $metaName, $metaValue), $post->id());
        }
    }
}