<?php

namespace AutoloadPost\Model\Post\UseCase\Save;

use AutoloadPost\Action\ValidationException;

class Command
{
    /**
     * @var array
     */
    public $posts;

    /**
     * Posts command constructor.
     * @param array $posts
     */
    public function __construct(array $posts)
    {
        $this->posts = $posts;
    }

    /**
     * @return array
     */
    public function validate()
    {
        $errors = [];
        foreach($this->posts as $key => $post) {
            $this->setMetaTags($key);
            $this->setDefaultValues($key);

            $post = $post['tags'];
            if(!isset($post['content']) || $post['content'] == '') {
                $errors[$key][] = "Content is required";
            }
            if(!isset($post['title']) || $post['title'] == '') {
                $errors[$key][] = "Title is required";
            }
        }

        return $errors;
    }

    /**
     * @param $key
     */
    private function setMetaTags($key)
    {
        foreach($this->posts[$key]['tags'] as $tagName => $tag) {
            if(substr($tagName, 0, 4) == 'meta') {
                $this->posts[$key]['tags']['metaTags'][$tagName] = $tag;
            }
        }
    }


    /**
     * @param $key
     * @return void
     */
    private function setDefaultValues($key)
    {
        if(!isset($this->posts[$key]['srcList'])) {
            $this->posts[$key]['srcList'] = [];
        }

        $post = $this->posts[$key]['tags'];

        if(!isset($post['date'])) {
            $this->posts[$key]['tags']['date'] = time();
        }
        if(!isset($post['type'])) {
            $this->posts[$key]['tags']['type'] = '';
        }
        if(!isset($post['status'])) {
            $this->posts[$key]['tags']['status'] = '';
        }
        if(!isset($post['category'])) {
            $this->posts[$key]['tags']['category'] = 'Uncategorized';
        }
        if(!isset($post['metaTags'])) {
            $this->posts[$key]['tags']['metaTags'] = [];
        }
    }
}