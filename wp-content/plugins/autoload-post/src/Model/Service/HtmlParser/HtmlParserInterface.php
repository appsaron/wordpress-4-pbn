<?php

namespace AutoloadPost\Model\Service\HtmlParser;

interface HtmlParserInterface
{
	public function set_text($text);
	public function get_all_parent_tags();
	public function remove_all_parent_tags();
	public function remove_tags_without_content(array $selectorList);
	public function get_root_html();
	public function get_html($selector);
	public function get($selector);
	public function get_childs($selector);
	public function for_each_tag($selector, $func);
	public function attr($selector, $attr);
	public function cut($selector);
}