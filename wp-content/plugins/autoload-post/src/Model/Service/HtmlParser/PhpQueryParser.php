<?php

namespace AutoloadPost\Model\Service\HtmlParser;

use PhpQuery\PhpQuery as phpQuery;

class PhpQueryParser implements HtmlParserInterface
{
	private $parsed_text;

	public function __construct()
	{
		phpQuery::use_function(__NAMESPACE__);
	}

	public function set_text($text)
	{
		$this->parsed_text = phpQuery::newDocument($text);
	}

	public function get_all_parent_tags()
	{
		$parent_tags = [];
		$all_tags = $this->parsed_text->find('*');
		foreach($all_tags as $tag) {
			if($tag->parentNode->nodeName == 'body') {
				$fullTag = pq($tag);
				$parent_tags[$tag->nodeName] = trim($fullTag->html());
			}
		}

		return $parent_tags;
	}

	public function remove_all_parent_tags()
	{
		$all_tags = $this->parsed_text->find('*');
		foreach($all_tags as $tag) {
			if($tag->parentNode->nodeName == 'body' && $tag->nodeName == 'p') {
				$tagObj = pq($tag);
				$tagObj->replaceWith($tagObj->html());
			}
		}
	}

	public function remove_tags_without_content(array $selectorList) {
		foreach($selectorList as $selector) {
			$this->for_each_tag($selector, function ($tag) {
				$tag->replaceWith($tag->html());
			});
		}
	}

	public function get_root_html()
	{
		return $this->parsed_text->html();
	}

	public function get_html($selector)
	{
		return $this->parsed_text->find($selector)->html();
	}

	public function get($selector) {
		return $this->parsed_text->find($selector);
	}

	public function get_childs($selector)
	{
		$childs = [];
		$parent_elem = $this->parsed_text->find($selector)->children();
		foreach($parent_elem as $elem) {
			$childs[$elem->nodeName] = $elem->nodeValue;
		}
		return $childs;
	}

	public function for_each_tag($selector, $func) {
		$all_tags = $this->parsed_text[$selector];
		foreach($all_tags as $tag) {
			$func(pq($tag));
		}
	}

	public function attr($selector, $attr)
	{
		if(trim($selector) == '') {
			throw new \Exception("Empty selector sended to attr function");
		}

		$elem = $this->parsed_text->find($selector);

		if($elem->length != 0) {
			$attr = $elem->attr($attr);
			$elem->remove();
			return $attr;
		}

		return '';
	}

	public function cut($selector)
	{
		if(trim($selector) == '') {
			throw new \Exception("Empty selector sended to cut function");
		}

		$elem = $this->parsed_text->find($selector);
		
		if($elem->length != 0) {
			$text = $elem->text();
			$elem->remove();
			return $text;
		}

		return '';
	}
}