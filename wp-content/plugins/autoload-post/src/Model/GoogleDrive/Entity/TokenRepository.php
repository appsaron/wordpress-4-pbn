<?php

namespace AutoloadPost\Model\GoogleDrive\Entity;

class TokenRepository
{
	public function save(AccessToken $token)
	{
		$_SESSION['access_token'] = $token->token();
	}

	public function get()
	{
		if(!isset($_SESSION['access_token'])) {
			return new AccessToken();
		}

		return new AccessToken($_SESSION['access_token']);
	}
}