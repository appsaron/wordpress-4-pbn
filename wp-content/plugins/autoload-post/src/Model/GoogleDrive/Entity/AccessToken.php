<?php

namespace AutoloadPost\Model\GoogleDrive\Entity;


class AccessToken
{
	private $token = [];

	public function __construct(array $token = [])
	{
		$this->setToken($token);
	}

	public function token()
	{
		return $this->token;
	}

	public function isActual()
	{
    	return !$this->isEmpty() && $this->moreThanFirtyMinutesLeft();
	}

	private function moreThanFirtyMinutesLeft()
	{
		return $this->token['created'] + $this->token['expires_in'] - time() > 40 * 60;
	}

	private function isEmpty()
	{
		return empty($this->token);
	}

	private function setToken(array $token = [])
	{
		if(!empty($token)) {
			$this->token = $token;
			$this->token['scope'] = str_replace('\\', '/', $this->token['scope']);
		}
	}
}