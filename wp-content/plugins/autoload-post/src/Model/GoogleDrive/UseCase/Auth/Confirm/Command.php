<?php

namespace AutoloadPost\Model\GoogleDrive\UseCase\Auth\Confirm;

class Command
{
	/**
	 * Access token from google drive api
	 */
	public $token;

	public function validate()
	{
		$keys = [
			'created', 
			'expires_in', 
			'access_token',
		];

		foreach($keys as $key) {
			if(array_key_exists($key, $this->token) === false) {
				return false;
			}
		}
		return true;
	}
}