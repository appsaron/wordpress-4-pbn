<?php

namespace AutoloadPost\Model\GoogleDrive\UseCase\Auth\Request;

use AutoloadPost\Model\GoogleDrive\Entity\TokenRepository;
use AutoloadPost\Model\GoogleDrive\Service\Requester;

class Handler
{
	private $tokens;
	private $requester;

	public function __construct(TokenRepository $tokens, Requester $requester)
	{
		$this->tokens = $tokens;
		$this->requester = $requester;
	}

	public function handle($command)
	{
		$url = $command->serverUrl . '?url=' . site_url();

		$accessToken = $this->tokens->get(); 
		
		if(!$accessToken->isActual()) {
			$this->requester->request($url);
		}
	}
}