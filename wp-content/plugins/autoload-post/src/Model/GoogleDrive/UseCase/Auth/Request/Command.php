<?php

namespace AutoloadPost\Model\GoogleDrive\UseCase\Auth\Request;

class Command
{
	/**
	 * URL for authorization
	 */
	public $serverUrl;
}