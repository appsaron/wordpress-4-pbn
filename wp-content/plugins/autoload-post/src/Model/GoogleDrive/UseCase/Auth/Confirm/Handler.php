<?php

namespace AutoloadPost\Model\GoogleDrive\UseCase\Auth\Confirm;

use AutoloadPost\Model\GoogleDrive\Entity\TokenRepository;
use AutoloadPost\Model\GoogleDrive\Entity\AccessToken;


class Handler
{
	private $tokens;

	public function __construct(TokenRepository $tokens)
	{
		$this->tokens = $tokens;
	}

	public function handle($command)
	{
		$token = new AccessToken($command->token);
		$this->tokens->save($token);
	}
}