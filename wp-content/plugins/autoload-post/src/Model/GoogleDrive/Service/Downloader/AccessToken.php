<?php

namespace AutoloadPost\Model\GoogleDrive\Service\Downloader;

class AccessToken
{
	private $token;

	public function __construct(array $token = [])
	{
		if(empty($token) || !$this->validate($token)) {
			throw new GoogleAuthException('Error Google Drive Authorization');
		}

		$this->token = $token;
	}

	public function token()
	{
		return $this->token;
	}

	private function validate($token)
	{
		$keys = [
			'created', 
			'expires_in', 
			'access_token'
		];

		foreach($keys as $key) {
			if(array_key_exists($key, $token) === false) {
				return false;
			}
		}
		return true;
	}
}