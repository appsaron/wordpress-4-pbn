<?php

namespace AutoloadPost\Model\GoogleDrive\Service\Downloader;

use AutoloadPost\Model\Service\HtmlParser\HtmlParserInterface;

class HtmlCleaner
{
	private $parser;
	private $html;

	public function __construct(HtmlParserInterface $parser)
	{
		$this->parser = $parser;
	}

	public function clean(File $file)
	{
		$this->html = $file->content;

		$body = $this->getHtmlBody();
		
		$this->parser->set_text($body);

		$this->changeSpanToRelateTag();

		$srcList = $this->getImageSrc();

		$this->deleteSpans();
		$this->deleteTagsAttr();

		$this->deleteAllRootPTags();

		$this->decodeHtmlspecialchars();

		$file->srcList = $srcList;
		$file->content = $this->parser->get_root_html();
		$file->tags = $this->parser->get_all_parent_tags();

		return $file;
	}

	private function getHtmlBody()
	{
		$this->parser->set_text($this->html);
		return $this->parser->get_html('body');
	}

	private function changeSpanToRelateTag()
	{
		$this->parser->for_each_tag('span', function ($span) {
			$attr = $span->attr('style');
			if(isset($attr) && strlen($attr) > 0) {
				$styles = explode(';', $attr);
				foreach($styles as $style) {
					$stt = explode(':', $style);
					if(trim($stt[0]) == 'font-style' && trim($stt[1]) == 'italic') {
						$span->replaceWith('<i>' . $span->html() . '</i>');
					}

					if(trim($stt[0]) == 'font-weight' && trim($stt[1]) == '700') {
						$span->replaceWith('<b>' . $span->html() . '</b>');
					}
				}
			}
		});
	}

	private function getImageSrc()
	{
		$srcList = [];
		$this->parser->for_each_tag('img', function ($image) use (&$srcList) {
			$srcList[] = $image->attr('src');
		});
		return $srcList;
	}

	private function deleteSpans()
	{
		$this->parser->remove_tags_without_content([
			'p > span',
			'h1 > span',
			'h2 > span',
			'h3 > span',
			'h4 > span',
			'h5 > span',
			'h6 > span',
			'li > span'
		]);
	}

	private function deleteTagsAttr()
	{
		$this->parser->set_text(preg_replace("/<([a-z][a-z0-9]*)[^>]*?(\/?)>/i",'<$1$2>', $this->parser->get_root_html()));
	}

	private function deleteAllRootPTags()
	{
		$this->parser->remove_all_parent_tags();
	}

	private function decodeHtmlspecialchars()
	{
		$this->parser->set_text(htmlspecialchars_decode($this->parser->get_root_html()));
	}
}