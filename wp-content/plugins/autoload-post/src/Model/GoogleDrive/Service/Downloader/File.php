<?php

namespace AutoloadPost\Model\GoogleDrive\Service\Downloader;

class File
{
	public $name;
	public $content;
	public $srcList;
	public $tags;

    /**
     * File constructor.
     * @param string $name
     * @param string $content
     */
    public function __construct(string $name, string $content)
	{
		$this->name = $name;
		$this->content = $content;
	}
}