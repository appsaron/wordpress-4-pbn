<?php

namespace AutoloadPost\Model\GoogleDrive\Service\Downloader;

class Downloader
{
	private $driveService;

	public function __construct(AccessToken $token)
	{
		$client = new \Google_Client();
        $client->setAccessToken($token->token());

        $this->driveService = new \Google_Service_Drive($client);
	}

	public function download($url)
	{
		$id = $this->getFileIdFromUrl($url);
        
		$file = $this->driveService->files->get($id);

		if($this->isCorrectMimeType($file->mimeType)) {
            return $this->downloadFile($file, $id);
        }
	}

	public function fileListFromFolder($url)
	{
		$id = $this->getFolderIdFromUrl($url);
		$files = [];
		$result = $this->driveService->files->listFiles(['q' => "\"{$id}\" in parents"]);

		foreach($result->files as $file) {
			$files[] = 'https://docs.google.com/document/d/' . $file->id . '/edit';
		}

		return $files;
	}

	private function downloadFile($file, $file_id)
	{
		$response = $this->getResponse($file->id);
        $content = $response->getBody()->getContents();

        $filename = $file->name;

        $content = str_replace('&nbsp;', ' ', $content);
        $content = str_replace('&quote;', '"', $content);

		$file = new File($filename, $content);
		return $file;
	}

	private function isCorrectMimeType($mimeType)
	{
		return $mimeType == 'application/vnd.google-apps.document';
	}

	private function getResponse($file_id)
	{
		return $this->driveService->files->export(
            $file_id, 
            'text/html',//application/vnd.openxmlformats-officedocument.wordprocessingml.document
            array(
              'alt' => 'media'
            )
        );
	}

	private function getFileIdFromUrl($url)
	{
		$needle = '/document/d/';
		$pos = strripos($url, $needle);

		$id = substr($url, $pos + strlen($needle));
		return explode('?', explode('/', $id)[0])[0];
	}

	private function getFolderIdFromUrl($url)
	{
		$needle = 'drive/folders/';
		$pos = strripos($url, $needle);

		$id = substr($url, $pos + strlen($needle));
		return explode('?', explode('/', $id)[0])[0];
	}
}