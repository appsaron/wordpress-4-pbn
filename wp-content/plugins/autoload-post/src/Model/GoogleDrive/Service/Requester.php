<?php

namespace AutoloadPost\Model\GoogleDrive\Service;

class Requester
{
	public function request($url)
	{
        header('Location: ' . filter_var($url, FILTER_SANITIZE_URL));
        exit;
	}
}