<?php

namespace AutoloadPost\Model\File\UseCase\Lists;

use AutoloadPost\Action\ValidationException;

class Command
{
    /**
     * @var string[] Lists of folder urls for download
     */
    public $folderURLs;

    private $errors;

    /**
     * @throws ValidationException
     */
    public function validate()
    {
        foreach($this->folderURLs as $url) {
            if(trim($url) == '') {
                unset($this->folderURLs[$key]);
                continue;
            }
            $pos = strripos($url, 'drive/folders/');

            if($pos === false) {
                $this->errors[] = 'Invalid folder url ' . $url;
            }
        }

        if(!empty($this->errors)) {
            throw new ValidationException($this->errors);
        }
    }
}