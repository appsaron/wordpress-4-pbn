<?php

namespace AutoloadPost\Model\File\UseCase\Lists;

use AutoloadPost\Model\File\Entity\FileRepository;
use AutoloadPost\Model\File\Service\Parser\Parser;

class Handler
{
    private $files;
    private $parser;

    public function __construct(FileRepository $files, Parser $parser)
    {
        $this->files = $files;
        $this->parser = $parser;
    }

    public function handle(Command $command)
    {
        $files = $this->files->getFileNamesFromFolder($command->folderURLs);

        return $files;
    }
}