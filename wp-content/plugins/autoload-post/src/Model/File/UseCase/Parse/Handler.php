<?php

namespace AutoloadPost\Model\File\UseCase\Parse;

use AutoloadPost\Model\File\Entity\FileRepository;
use AutoloadPost\Model\File\Service\Parser\Parser;

class Handler
{
	private $files;
	private $parser;

	public function __construct(FileRepository $files, Parser $parser)
	{
		$this->files = $files;
		$this->parser = $parser;
	}

	public function handle(Command $command)
	{
		$files = $this->files->getAllByUrls($command->fileURLs, $command->folderURLs);

		foreach($files as $file) {
			$file->setTags($this->parser->parse($file->content()));
		}

		return $files;
	}
}