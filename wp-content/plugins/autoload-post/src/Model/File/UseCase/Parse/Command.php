<?php

namespace AutoloadPost\Model\File\UseCase\Parse;

use AutoloadPost\Action\ValidationException;

class Command
{
	/**
	 * @var string[] Lists of file urls for download
	 */
	public $fileURLs;

	/**
	 * @var string[] Lists of folder urls for download
	 */
	public $folderURLs;

	private $errors;

	public function validate()
	{
		foreach($this->fileURLs as $key => $url) {
			if(trim($url) == '') {
				unset($this->fileURLs[$key]);
				continue;
			} 
			$pos = strripos($url, '/document/d/');

			if($pos === false) {
				$this->errors[] = 'Invalid file url ' . $url;
			}
		}

		foreach($this->folderURLs as $url) {
			if(trim($url) == '') {
				unset($this->folderURLs[$key]);
				continue;
			} 
			$pos = strripos($url, 'drive/folders/');

			if($pos === false) {
				$this->errors[] = 'Invalid folder url ' . $url;
			}
		}

		if(!empty($this->errors)) {
			throw new ValidationException($this->errors);
		}
	}
}