<?php

namespace AutoloadPost\Model\File\Service\Parser;

use AutoloadPost\Model\Service\HtmlParser\HtmlParserInterface;

class Parser
{
	private $parser;

	public function __construct(HtmlParserInterface $parser)
	{
		$this->parser = $parser;
	}

	public function parse(string $html) {
		$this->parser->set_text($html);
		return $this->parser->get_all_parent_tags();
	}
}