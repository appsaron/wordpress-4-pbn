<?php

namespace AutoloadPost\Model\File\Entity;

interface FileRepository
{
	public function getAllByUrls(array $fileUrls, array $folderUrls);

    public function getFileNamesFromFolder(array $folderUrls);
}