<?php

namespace AutoloadPost\Model\File\Entity;

class File 
{
	private $name;
	private $content = '';
	private $tags = [];
	private $srcList = [];

	public function __construct(string $name, string $content, array $srcList = [])
	{
		if(empty($name)) {
			throw new FileException("File name can't be empty");
		}

		$this->name = $name;
		$this->content = $content;
		$this->srcList = $srcList;
	}

	public function setTags(array $tags = [])
	{
		$this->tags = $tags;
	}

	public function name()
	{
		return $this->name;
	}

	public function srcList()
	{
		return $this->srcList;
	}

	public function content()
	{
		return $this->content;
	}

	public function tags()
	{
		return $this->tags;
	}
}