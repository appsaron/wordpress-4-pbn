jQuery(document).ready(function($) {
    if (!Array.prototype.last){
        Array.prototype.last = function(){
            return this[this.length - 1];
        };
    };

    var ajax = new Ajax();
    var classList = new ClassList();

    var posts = new Posts();
    var driveForm = new DriveForm();

    function DriveForm() {
        this.form = document.querySelector('#parse_drive');
        this.button = this.form.querySelector('.tpl-submit-button');
        this.loadImage = this.button.querySelector('img');
        this.foldersInput = this.form.querySelector('#drive-folder-input');
        this.filesInput = this.form.querySelector('#drive-file-input');

        this.alerts = new Alerts(this.form);

        this.init = function () {
            this.form.addEventListener('submit', this.submitHandler);
        }

        this.submitHandler = function(evt) {
            evt.preventDefault();
            this.showLoadImage();

            if(this.foldersInput.value !== '') {
                var formData = ajax.formData(this.form.id);
                formData.set('action', "files_list");

                ajax.sendAjax(
                    (res) => {
                        if(res.length == 0) return;
                        var result = JSON.parse(res);

                        if(result.files) {
                            result.files.forEach(function (item) {
                                this.filesInput.value = this.filesInput.value + "\n" + item;
                            }.bind(this));
                            
                            var formData = ajax.formData(this.form.id);
                            
                            var files = formData.get('drive-files').split("\n");
                
                            var newFormData = ajax.formData(this.form.id);
                            newFormData.set('drive-files', files.splice(0, Math.min(5, files.length)).join("\n"));
                                
                            this.loadFilesStack(newFormData, files);
                        }
                    },
                    () => {},
                    formData
                );
            } else {
                ajax.sendAjax(
                    (res) => {
                        this.hideLoadImage();
                        this.clear();
                        
                        this.driveLoadSuccess(res);
                    },
                    this.driveLoadError,
                    ajax.formData(this.form.id)
                );
            }

            
        }.bind(this);
        
        this.loadFilesStack = function(newFormData, files) {
            ajax.sendAjax(
                (res) => {
                    this.driveLoadSuccess(res);
                    console.log(files);
                    if(files.length > 0) {
                        var newFormData = ajax.formData(this.form.id);
                        newFormData.set('drive-files', files.splice(0, Math.min(5, files.length)).join("\n"));
                        
                        this.filesInput.value = files.join("\n");
                        
                        this.loadFilesStack(newFormData, files);
                    } else {
                        this.hideLoadImage();
                        this.clear();
                    }
                    
                },
                this.driveLoadError,
                newFormData
            );
        }.bind(this)

        this.showLoadImage = function () {
            classList.remove(this.loadImage, 'tpl-hidden');
        }

        this.clear = function () {
            var textAreas = this.form.querySelectorAll('textarea');
            for(var i = 0; i < textAreas.length; i++) {
                textAreas[i].value = '';
            }
        }

        this.driveLoadSuccess = function (res) {
            if(res.length == 0) return;
            var result = JSON.parse(res);
            if(result.files) {
                result.files.forEach(function (item) {
                    posts.createPost(item.fileName, item.content, item.tags, item.srcList);
                });
            }
        }.bind(this);

        this.driveLoadError = function () {
            this.alerts.clear();
            this.alerts.showErrors(['Something went wrong']);
            this.hideLoadImage();
        }.bind(this);

        this.hideLoadImage = function () {
            classList.add(this.loadImage, 'tpl-hidden');
        }

        this.init();
    }

    function Alerts(elementToPaste) {
        this.alerts = [];

        this.clear = function () {
            this.alerts.forEach(function (alert) {
                if(alert) {
                    alert.parentNode.removeChild(alert);
                }
            });
            this.alerts = [];
        }

        this.showErrors = function (errors) {
            errors.forEach(function(error) {
                this._create('error', error, elementToPaste);
            }.bind(this));
        }

        this._create = function(type, message, parent) {
            var alert = this.createElement(type, message);
            parent.appendChild(alert);
            this.alerts.push(alert);
            return alert;
        }

        this.showSuccess = function (message) {
            this._create('success', message, elementToPaste);
        }

        this.replaceElemWithAlert = function (parent, elem, type, text) {
            var alert = this._create(type, text, parent);
            parent.replaceChild(alert, elem);
        }

        this.createElement = function (type, text) {
            var div = document.createElement('div');
            var message = document.createElement('p');
            classList.add(div, 'tpl-alert');
            classList.add(div, 'tpl-alert--' + type);
            classList.add(message, 'tpl-alert--text');

            message.innerText = text;

            var buttonWrapper = document.createElement('div');
            classList.add(buttonWrapper, 'tpl-alert--close-buttons');

            var closeButton = document.createElement('button');
            closeButton.type = 'button';
            classList.add(closeButton, 'tpl-alert--close-button');

            closeButton.addEventListener('click', function (evt) {
                this.deleteAlert(evt.target.parentNode.parentNode);
            }.bind(this));

            div.appendChild(message);
            buttonWrapper.appendChild(closeButton);
            div.appendChild(buttonWrapper);

            return div;
        }

        this.deleteAlert = function (alertToDelete) {
            alertToDelete.parentNode.removeChild(alertToDelete);

            this.alerts.forEach(function (alert, key) {
                if(alertToDelete == alert) {
                    this.alerts.splice(key, 1);
                }
            }.bind(this));
        }
    }

    function Selector(element, postEntity) {
        this.element = element;
        this.postEntity = postEntity;

        this.inputHandler = function (selectorName) {
            this.postEntity.changeSelectorName(selectorName);
            this.element.value = selectorName;
        }
    }

    function SelectorList(post, postEntity) {
        this.parent = post.element.querySelector('.selector-list');

        this.selectors = [];

        this.addSelector = function () {
            var selector = new Selector(
                this.createElement(),
                postEntity.createNew()
            );

            this.selectors.push(selector);
            return selector;
        }.bind(this);

        this.getList = function () {
            var list = [];
            this.selectors.forEach(function (item, i, arr) {
                list.push({
                    selector: item.element.value,
                    entity: item.postEntity.value
                });
            });

            return list;
        }

        this.createElement = function () {
            var input = document.createElement('input');

            input.classList.add('tpl-input');
            input.type = 'text';
            input.name = 'selector';

            input.addEventListener('input', this.inputHandler);

            this.parent.appendChild(input);
            return input;
        }

        this.inputHandler = function (evt) {
            this.selectors.forEach(function(item, i, arr) {
                if(evt.target == item.element) {
                    item.inputHandler(evt.target.value);
                }
            });
        }.bind(this)
    }

    function NamePost(name, post) {
        this.name = name;
        this.namePost = post.element.querySelector('.tpl-block--title-text');
        this.nameInput = post.element.querySelector('.tpl-block--title-input');

        this.init = function () {
            this.nameInput.value = name;
            this.namePost.addEventListener('dblclick', this.doubleClickOnNameHandler);
            this.nameInput.addEventListener('input', this.changeNameHandler);
            this.nameInput.addEventListener('blur', this.blurHandler);
        }

        this.blurHandler = function () {
            classList.remove(post.titleElement, 'tpl-block--title__change');
        }.bind(this);

        this.doubleClickOnNameHandler = function () {
            classList.add(post.titleElement, 'tpl-block--title__change');
            this.nameInput.focus();
        }.bind(this);

        this.changeNameHandler = function (evt) {
            this._changeName(evt.target.value);
        }.bind(this);

        this._changeName = function (name) {
            this.name = name;
            this.namePost.innerText = name;
        }

        this.init();
    }

    function Post(element, postName, content, tags, posts, srcList) {
        this.element = element;
        this.buttonNewSelector = element.querySelector('.tpl-add-selector');
        this.titleElement = element.querySelector('.tpl-block--title');
        this.name = new NamePost(postName, this);
        this.content = content;
        this.tags = tags;
        this.postEntity = new PostEntity(this);
        this.selectorList = new SelectorList(this, this.postEntity);
        this.alerts = new Alerts(element.querySelector('.tpl-alerts-block'));
        this.srcList = srcList;

        this.makeActive = function (evt) {
            if(evt && evt.target.classList.contains('tpl-block--title') && evt.target.parentNode != posts.activePost.element) {
                posts.activePost.deactivate();
                this.activate();
            } else if(!evt) {
                this.activate();
            }
        }.bind(this);

        this.changeId = function () {
            var idInArray = this.element.id.split('-');
            idInArray.last = idInArray.last - 1;
            this.element.id = idInArray.join('-');
        }

        this.activate = function () {
            classList.add(this.element, 'tpl-block__active');
            var body = this.element.querySelector('.tpl-block--body');
            $(body).slideDown(400);
            posts.activePost = this;
        }

        this.makeActiveOnInit = function () {
            if(posts.activePost.null) {
                this.makeActive(false);
            }
        }

        this.init = function () {
            this.buttonNewSelector.addEventListener('click', this.selectorList.addSelector);
            this.element.querySelector('.tpl-post-save-button').addEventListener('click', this.savePostHandler);
            this.element.querySelector('.tpl-post-copy-button').addEventListener('click', this.copySettings);
            this.makeActiveOnInit();
            this.titleElement.addEventListener('click', this.makeActive);
        }

        this.copySettings = function () {
            posts.posts.forEach(function (post) {
                if(post !== this) {
                    post.setSettings(this.selectorList.getList());
                }
            }.bind(this));
        }.bind(this);

        this.setSettings = function (settingsList) {
            var currentSettings = this.selectorList.getList();
            settingsList.forEach(function (setting) {
                var defined = false;
                currentSettings.forEach(function (currentSetting) {
                    if(setting.entity == currentSetting.entity) {
                        defined = true;
                    }
                });
                if(!defined) {
                    var selector = this.selectorList.addSelector();
                    selector.inputHandler(setting.selector);
                    this.postEntity.buttons.activate(selector.postEntity, setting.entity);
                }
            }.bind(this));
        }

        this.deactivate = function () {
            classList.remove(this.element, 'tpl-block__active');
            var body = this.element.querySelector('.tpl-block--body');
            $(body).slideUp(400);
        }

        this.getPostData = function () {
            var postData = {
                postContent: this.content,
                tags: this.postEntity.buttons.preview.getValues(),
                srcList: this.srcList
            };

            postData.tags.title = this.name.name;

            return postData;
        }.bind(this);

        this.savePostHandler = function (evt) {
            var data = {
                posts: [this.getPostData()],
                action: 'save_post'
            };

            this.showLoadImage(evt.target);
            ajax.sendAjax(
                function (res) { this.postSaveSuccess(evt, res); }.bind(this),
                function (res) { this.postSaveError(evt, res); }.bind(this),
                data,
                false
            );
        }.bind(this);

        this.postSaveError = function (evt, res) {
            this.hideLoadImage(evt.target);
            this.alerts.clear();
            this.alerts.showErrors(['Something went wrong']);
        }.bind(this);

        this.postSaveSuccess = function (evt, res) {
            this.hideLoadImage(evt.target);

            this.alerts.clear();

            if(res.length == 0) return;
            res = JSON.parse(res);
            res = res[0];
            if(res.success) {
                posts.deletePost(this);

            }
            if(res.errors) {
                this.alerts.showErrors(res.errors);
            }
        }.bind(this);

        this.showLoadImage = function (element) {
            if(element.tagName != 'button') {
                element = element.parentNode;
            }
            classList.remove(element.querySelector('#tpl-img-load'), 'tpl-hidden');
            classList.add(element.querySelector('#tpl-img-disc'), 'tpl-hidden');
        }

        this.hideLoadImage = function (element) {
            if(element.tagName != 'button') {
                element = element.parentNode;
            }
            classList.add(element.querySelector('#tpl-img-load'), 'tpl-hidden');
            classList.remove(element.querySelector('#tpl-img-disc'), 'tpl-hidden');
        }

        this.getTag = function (tagName) {
            if(this.tags[tagName]) {
                return this.tags[tagName];
            } else {
                return undefined;
            }
        }

        this.init();
    }

    function Posts() {
        this.parent = document.querySelector('.tpl-posts');

        this.posts = [];

        this.activePost = {
            null: true
        };

        this.buttonSaveAll = document.querySelector('#tpl-button-save-all');

        this.init = function () {
            this.buttonSaveAll.addEventListener('click', this.saveAll);
        }

        this.saveAll = function (evt) {
            var postsToSave = [];

            this.posts.forEach(function (post) {
                postsToSave.push(post.getPostData());
            });

            var data = {
                posts: postsToSave,
                action: 'save_post'
            };

            // this.showLoadImage(evt.target);
            ajax.sendAjax(
                function (res) { this.postsSaveSuccess(evt, res); }.bind(this),
                function (res) { this.postsSaveError(evt, res); }.bind(this),
                data,
                false
            );
        }.bind(this);

        this.postsSaveSuccess = function (evt, res) {
            if(res.length == 0) return;
            res = JSON.parse(res);

            var postsToDelete = [];

            this.posts.forEach(function (post, key) {
                if(res[key].success) {
                    postsToDelete.push(post);
                }
                if(res[key].errors) {
                    post.alerts.clear();
                    post.alerts.showErrors(res[key].errors);
                }
            }.bind(this));

            postsToDelete.forEach(function (post) {
                this.deletePost(post);
            }.bind(this));
        }

        this.postSaveError = function (evt, res) {
            alert('Something went wrong');
        }

        this.createPost = function (postName, content, tags, srcList) {
            var post = new Post(
                this.createElement(postName),
                postName,
                content,
                tags,
                this,
                srcList
            );

            this.posts.push(post);
            return post;
        }

        this.deletePost = function (post) {
            var postDeleted = false;
            this.posts.forEach(function (postItem, key) {
                if(postDeleted) {
                    postItem.changeId();
                }
                if(post == postItem) {
                    this.posts.splice(key, 1);
                    postDeleted = true;
                }
            }.bind(this));
            if(postDeleted) {
                post.alerts.replaceElemWithAlert(
                    post.element.parentNode,
                    post.element,
                    'success',
                    'Post "' + post.name.name + '" has been saved'
                );
            }
        }

        this.createElement = function (postName) {
            var temp = document.querySelector('#tpl-post-template');
            var post = temp.content.cloneNode(true);
            var id = 'tpl-post-' + this.posts.length;
            post.querySelector('.tpl-block').id = id;

            post.querySelector('.tpl-block--title-text').innerText = postName;

            classList.remove(this.buttonSaveAll, 'tpl-hidden');
            this.parent.appendChild(post);
            return this.parent.querySelector('#' + id);
        }

        this.init();
    }

    function Buttons(postEntity, post) {
        this.post = post;
        this.preview = new Preview(post);

        this.buttons = [];

        this.createElement = function (innerText) {
            var button = document.createElement('button');
            button.addEventListener('click', this.clickHandler);
            button.innerText = innerText;
            button.classList.add('tpl-post-entity--button');

            this.buttons.push(button);

            return button;
        }

        this.makeActive = function (button, entity) {
            if(entity.activeButton.classList) {
                classList.remove(entity.activeButton, 'tpl-post-entity--button__active');
            }
            classList.add(button, 'tpl-post-entity--button__active');
            entity.activeButton = button;
        }

        this.activate = function (entity, text) {
            var button = this.getButtonByText(text);
            if(button) {
                entity.setValue(text);
                this.makeActive(button, entity);
                this.preview.add(entity.selectorName, this.post.getTag(entity.selectorName), entity.value);
            }
        }

        this.getButtonByText = function (text) {
            var result;
            this.buttons.forEach(function (button) {
                if(button.innerText == text) {
                    result = button;
                }
            });
            return result;
        }

        this.clickHandler = function (evt) {
            postEntity.entities.forEach(function (entity, i) {
                if(entity.element == evt.target.parentNode.parentNode) {
                    this.activate(entity, evt.target.innerText);
                }
            }.bind(this));
        }.bind(this)
    }

    function Tag(tagName, entityName, element) {
        this.tagName = tagName;
        this.entityName = entityName;
        this.element = element;
        this.textarea = element.querySelector('textarea');
        this.error = element.querySelector('.error');
        this.labels = [element.querySelector('.tpl-preview--open-tag'),  element.querySelector('.tpl-preview--close-tag')];

        this.addResult = function (result) {
            if(this.content !== undefined) {
                result[this.entityName] = this.content;
            }

            return result;
        }

        this.changeContent = function (content) {
            if(content !== undefined) {
                classList.remove(this.textarea, 'tpl-hidden');
                classList.add(this.error, 'tpl-hidden');

                this.content = content;
                this.textarea.value = content;
            } else {
                this.content = undefined;
                classList.add(this.textarea, 'tpl-hidden');
                classList.remove(this.error, 'tpl-hidden');

                this.error.innerText = 'Указан неверный селектор';
            }
        }

        this.changeTagName = function (tagName) {
            this.tagName = tagName;
            this.labels.forEach(function (label) {
                this.changeLabel(label, tagName);
            }.bind(this));
        }

        this.changeLabel = function (label, tagName) {
            label.innerText = '<' + tagName + '>';
        }

        this.reloadContent = function () {
            this.content = textarea.value;
        };
    }

    function Preview(post) {
        this.parent = post.element.querySelector('.tpl-previews');
        this.tags = [];
        this.post = post;

        this.getValues = function () {
            var result = {};
            this.tags.forEach(function(tag) {
                result = tag.addResult(result);
            });
            return result;
        }

        this.changeTagName = function (tagName, oldTagName) {
            this.tags.forEach(function(tag) {
                if(tag.tagName == oldTagName) {
                    tag.changeContent(this.post.getTag(tagName));
                    tag.changeTagName(tagName);
                }
            }.bind(this));
        }

        this.add = function (tagName, content, entityName) {
            this.show();

            var created = false;
            var tag = {};
            this.tags.forEach(function(item) {
                if(item.tagName == tagName) {
                    tag = item;
                    created = true;
                }
            });

            if(created) {
                tag.entityName = entityName;
                tag.content = content;
            } else {
                tag = new Tag(tagName, entityName, this.createElement(tagName, content));
                tag.changeContent(content);

                this.tags.push(tag);
            }
        }

        this.show = function () {
            classList.remove(this.parent.parentNode, 'tpl-hidden');
        }

        this.createElement = function (tagName, content) {
            var temp = document.querySelector('#tpl-preview-template');
            var preview = temp.content.cloneNode(true);

            var id = 'tag-' + tagName + '-' + this.tags.length;
            preview.querySelector('label').id = id;
            preview.querySelector('label .tpl-preview--open-tag').innerText = '<' + tagName + '>';
            preview.querySelector('label .tpl-preview--close-tag').innerText = '</' + tagName + '>';
            preview.querySelector('textarea').value = content;

            preview.querySelector('textarea').addEventListener('input', this.inputHandler);

            this.parent.appendChild(preview);

            return this.parent.querySelector('#' + id);
        }

        this.inputHandler = function (evt) {
            this.tags.forEach(function (tag) {
                tag.reloadContent();
            });
        }.bind(this);
    }

    function Entity(postEntity) {
        this.element = postEntity.createElement();
        this.activeButton = {};
        this.value = '';
        this.selectorName = '';
        this.title = this.element.querySelector('.tpl-post-entity--title');
        this.buttons = this.element.querySelector('.tpl-post-entity--buttons');

        this.setValue = function (value) {
            this.value = value;
        }

        this.changeSelectorName = function (selectorName) {
            this.title.innerText = postEntity.getSelectorName(selectorName);
            postEntity.changeSelectorName(selectorName, this.selectorName);
            this.selectorName = selectorName;
        }

        this.makeActive = function () {
            if(postEntity.activeEntity != this) {
                $(this.buttons).slideDown(400);
                classList.add(this.element, 'tpl-post-entity__active');
                postEntity.activeEntity.deactivate();
                postEntity.activeEntity = this;
            }
        }

        this.deactivate = function () {
            $(this.buttons).slideUp(400);
            classList.remove(this.element, 'tpl-post-entity__active');
        }
    }

    function PostEntity(post) {
        this.parent = post.element.querySelector('.post-entities');

        this.buttons = new Buttons(this, post);

        this.postEntities = [
            'title',
            'content',
            'category',
            'meta_keywords',
            'meta_description'
        ];

        this.entities = [];

        this.activeEntity = {
            deactivate: function () {}
        };

        this.createNew = function () {
            this.show();

            var entity = new Entity(this);

            entity.makeActive();
            this.entities.push(entity);
            return entity;
        }

        this.show = function () {
            classList.remove(this.parent.parentNode.parentNode, 'tpl-hidden');
        }

        this.changeSelectorName = function (selectorName, oldSelectorName) {
            this.buttons.preview.changeTagName(selectorName, oldSelectorName);
        }

        this.getSelectorName = function (selector) {
            return 'For <' + selector + '>';
        }

        this.createElement = function () {
            var div = document.createElement('div');
            div.classList.add('tpl-post-entity');
            var buttonsDiv = document.createElement('div');
            buttonsDiv.classList.add('tpl-post-entity--buttons');

            var p = document.createElement('p');
            p.classList.add('tpl-post-entity--title');
            p.addEventListener('click', this.clickOnTitleHandler);
            p.innerText = this.getSelectorName('{empty}');

            div.appendChild(p);

            this.postEntities.forEach(function (item, i, arr) {
                buttonsDiv.appendChild(this.buttons.createElement(item));
            }.bind(this));

            div.appendChild(buttonsDiv);


            this.parent.appendChild(div);
            return div;
        }

        this.clickOnTitleHandler = function (evt) {
            this.entities.forEach(function (entity) {
                if(entity.element == evt.target.parentNode) {
                    entity.makeActive();
                }
            });
        }.bind(this);
    }

    function Ajax() {
        this.sendAjax = function(successFunction, errorFunction, formData, fromForm = true) {
            var params = {
                method: "POST",
                url: paths.ajaxPath,

                data: formData,
                success: function(res) {
                    successFunction(res);
                },
                error: function() {
                    errorFunction();
                }
            }
            if(fromForm) {
                params.processData = false;
                params.contentType = false;
            }
            $.ajax(params);
        }

        this.formData = function (formId) {
            var formData = new FormData(
                document.querySelector('#' + formId)
            );
            formData.append('action', formId);
            return formData;
        }
    }

    function ClassList() {
        this.remove = function (element, className) {
            if(element.classList.contains(className)) {
                element.classList.remove(className);
            }
        }

        this.add = function (element, className) {
            if(!element.classList.contains(className)) {
                element.classList.add(className);
            }
        }
    }
});