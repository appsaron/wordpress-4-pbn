<?php

namespace AutoloadPost\Action\GoogleDrive\Auth;

use AutoloadPost\Model\GoogleDrive\UseCase\Auth\Confirm\Handler;
use AutoloadPost\Model\GoogleDrive\UseCase\Auth\Confirm\Command;
use AutoloadPost\Action\ValidationException;
use AutoloadPost\Action\Action;

class ConfirmAction extends Action
{
	private $handler;

	public function __construct(Handler $handler)
	{
		$this->handler = $handler;
	}

	public function handle()
	{
		$command = new Command();
		$command->token = $_POST['access_token'];

		if(!$command->validate()) {
			throw new ValidationException(["Invalid Google Access Token"]);
		}

		$this->handler->handle($command);
	}
}