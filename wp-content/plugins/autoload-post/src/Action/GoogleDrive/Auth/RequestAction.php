<?php

namespace AutoloadPost\Action\GoogleDrive\Auth;

use AutoloadPost\Model\GoogleDrive\UseCase\Auth\Request\Handler;
use AutoloadPost\Model\GoogleDrive\UseCase\Auth\Request\Command;
use AutoloadPost\Action\Action;

class RequestAction extends Action
{
	private $handler;
    private $config;

    public function __construct(Handler $handler, array $config)
	{
		$this->handler = $handler;
        $this->config = $config;
    }

	public function handle()
	{
		$command = new Command();
		$command->serverUrl = $this->config['authUrl'];
		
		$this->handler->handle($command);
	}
}