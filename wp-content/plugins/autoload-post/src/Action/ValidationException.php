<?php

namespace AutoloadPost\Action;

class ValidationException extends \Exception
{
	protected $errors = array();

	public function __construct(array $errors) {
		$this->errors = $errors;
	}

	public function getErrors()
	{
		return $this->errors;
	}
}