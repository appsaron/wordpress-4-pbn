<?php

namespace AutoloadPost\Action\File;

use AutoloadPost\Model\File\UseCase\Parse\Command;
use AutoloadPost\Model\File\UseCase\Parse\Handler;
use AutoloadPost\Action\Action;

class ParseAction extends Action
{
	private $handler;

	public function __construct(Handler $handler)
	{
		$this->handler = $handler;
	}

	public function handle()
	{
		$command = new Command();

		$command->fileURLs = explode("\n", $_POST['drive-files']);
		foreach($command->fileURLs as $index => $file) {
		    if(trim($file) == '') {
		        unset($command->fileURLs[$index]);
            }
        }
		$command->folderURLs = explode("\n", $_POST['drive-folder']);
		

		$command->validate();

		$files = $this->handler->handle($command);
		$result = [];
		foreach($files as $file) {
			$result[] = [
				'tags' => $file->tags(),
				'fileName' => $file->name(),
				'content' => $file->content(),
				'srcList' => $file->srcList()
			];
		}
		$this->showJson(['files' => $result]);
	}
}