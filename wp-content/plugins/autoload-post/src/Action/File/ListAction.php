<?php

namespace AutoloadPost\Action\File;

use AutoloadPost\Model\File\UseCase\Lists\Handler;
use AutoloadPost\Model\File\UseCase\Lists\Command;
use AutoloadPost\Action\Action;

class ListAction extends Action
{
    private $handler;

    public function __construct(Handler $handler)
    {
        $this->handler = $handler;
    }

    /**
     * @throws \AutoloadPost\Action\ValidationException
     */
    public function handle()
    {
        $command = new Command();

        $command->folderURLs = explode("\n", $_POST['drive-folder']);

        $command->validate();

        $files = $this->handler->handle($command);

        $this->showJson(['files' => $files]);
    }
}