<?php

namespace AutoloadPost\Action;

abstract class Action
{
	protected function showJson($array)
	{
		print_r(json_encode($array));
		exit;
	}
}