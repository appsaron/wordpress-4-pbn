<?php

namespace AutoloadPost\Action\Post;

use AutoloadPost\Model\Post\UseCase\Save\Handler;
use AutoloadPost\Model\Post\UseCase\Save\Command;
use AutoloadPost\Action\Action;

class SaveAction extends Action
{
	private $handler;

	public function __construct(Handler $handler)
	{
		$this->handler = $handler;
	}

	public function handle()
	{
	    $posts = isset($_POST['posts']) ? $_POST['posts'] : [];
		$command = new Command($posts);

		$validationErrors = $command->validate();

		$this->handler->setErrors($validationErrors);
		$this->handler->handle($command);
		$errors = $this->handler->errors();

        $result = $this->prepareResult($errors, $posts);

		$this->showJson($result);
	}

    /**
     * @param array $errors
     * @param array $posts
     * @return array
     */
    private function prepareResult(array $errors, array $posts)
    {
        $result = [];
        foreach($posts as $postNumber => $post) {
            if(!isset($errors[$postNumber])) {
                $result[$postNumber]['success'] = 'Post added success';
            } else {
                $result[$postNumber]['errors'] = $errors[$postNumber];
            }
        }
        return $result;
    }
}