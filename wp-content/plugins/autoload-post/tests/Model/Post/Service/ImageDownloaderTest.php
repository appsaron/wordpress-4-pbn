<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;

use AutoloadPost\Model\Post\Service\ImageDownloader\ImageDownloaderInterface;
use AutoloadPost\Model\Post\Service\ImageDownloader\Images;
use AutoloadPost\Model\Post\Service\ImageDownloader\SrcList;
 

final class ImageDownloaderTest extends TestCase
{
    public function testCreateNewSrcList(): void
    {   
        $images = new Images();
        $srcList = new SrcList([
            'http://google.com/1.jpg',
            'http://google.com/2.jpg',
            'http://google.com/3.jpg'
        ]);

        $newSrcList = $images->downloadImages($srcList, new DummyDownloader());

        $this->assertEquals(
            new SrcList([
                'http://my-domen.com/1.jpg',
                'http://my-domen.com/1.jpg',
                'http://my-domen.com/1.jpg'
            ]),
            $newSrcList
        );
    }

    public function testChangeAllImgSrc(): void
    {   
        $images = new Images();
        $srcList = new SrcList([
            'http://google.com/1.jpg',
            'http://google.com/2.jpg',
            'http://google.com/3.jpg'
        ]);

        $html = '<h1>Header</h1><p><img></p><span><img></span><img>';

        $changedImg = $images->changeImgTag($html, $srcList);

        $this->assertEquals(
            '<h1>Header</h1><p><img src="http://google.com/1.jpg"></p><span><img src="http://google.com/2.jpg"></span><img src="http://google.com/3.jpg">',
            $changedImg
        );
    }
}

class DummyDownloader implements ImageDownloaderInterface
{
    public function download(string $url)
    {
        return 'http://my-domen.com/1.jpg';
    }
}