<?php

use Pimple\Container;

use AutoloadPost\Action\GoogleDrive\Auth\RequestAction;
use AutoloadPost\Action\GoogleDrive\Auth\ConfirmAction;
use AutoloadPost\Action\File\ParseAction;

use AutoloadPost\Action\File\ListAction;

$container = new Container();

require('config.php');

use AutoloadPost\Model\Post\Entity\PostRepository;
use AutoloadPost\Model\Post\Entity\CategoryRepository;
use AutoloadPost\Model\Post\Entity\MetaTagsRepository;
use AutoloadPost\Model\Post\Service\ImageDownloader\DownloadService;
use AutoloadPost\Model\Post\Service\ImageDownloader\ImageDownloaderInterface;

$container['MetaTagsRepositoryClass'] = \AutoloadPost\Infrastructure\Model\Post\Entity\WordpressMetaTagsRepository::class;

$container[MetaTagsRepository::class] = function ($c) {
    return new $c['MetaTagsRepositoryClass']();
};

$container['ImageDownloaderClass'] = \AutoloadPost\Model\Post\Service\ImageDownloader\WordpressImageDownloader::class;

$container[ImageDownloaderInterface::class] = function($c) {
    return new $c['ImageDownloaderClass']($c['config']['imageDownload']);
};

$container['CategoryRepositoryClass'] = \AutoloadPost\Infrastructure\Model\Post\Entity\WordpressCategoryRepository::class;

$container[CategoryRepository::class] = function($c) {
    return new $c['CategoryRepositoryClass']();
};

$container['PostRepositoryClass'] = \AutoloadPost\Infrastructure\Model\Post\Entity\WordpressPostRepository::class;

$container[PostRepository::class] = function($c) {
    return new $c['PostRepositoryClass']();
};

$container[DownloadService::class] = function () {
    return new DownloadService();
};

$container[\AutoloadPost\Model\Post\UseCase\Save\Handler::class] = function ($c) {
    return new \AutoloadPost\Model\Post\UseCase\Save\Handler(
        $c[PostRepository::class],
        $c[CategoryRepository::class],
        $c[MetaTagsRepository::class],
        $c[DownloadService::class],
        $c[ImageDownloaderInterface::class]
    );
};

$container[AutoloadPost\Action\Post\SaveAction::class] = function ($c) {
    return new AutoloadPost\Action\Post\SaveAction(
        $c[\AutoloadPost\Model\Post\UseCase\Save\Handler::class]
    );
};

//-----------------------------------------

$container[RequestAction::class] = function ($c) {
	return new RequestAction(
		new \AutoloadPost\Model\GoogleDrive\UseCase\Auth\Request\Handler(
			new AutoloadPost\Model\GoogleDrive\Entity\TokenRepository(),
			new AutoloadPost\Model\GoogleDrive\Service\Requester()
		),
        $c['config']['drive']
	);
};

$container[ConfirmAction::class] = function ($c) {
	return new ConfirmAction(
		new \AutoloadPost\Model\GoogleDrive\UseCase\Auth\Confirm\Handler(
			new \AutoloadPost\Model\GoogleDrive\Entity\TokenRepository()
		)
	);
};


//-------------------------------------

$container['FileRepositoryClass'] = '\AutoloadPost\Infrastructure\Model\File\Entity\DriveFileRepository';

$container['AutoloadPost\Model\File\Entity\FileRepository'] = function ($c) {
	return new $c['FileRepositoryClass'];
};

$container['HtmlParserClass'] = '\AutoloadPost\Model\Service\HtmlParser\PhpQueryParser';

$container['AutoloadPost\Model\Service\HtmlParser\HtmlParserInterface'] = function ($c) {
	return new $c['HtmlParserClass']();
};

$container['AutoloadPost\Model\File\Service\Parser\Parser'] = function ($c) {
	return new \AutoloadPost\Model\File\Service\Parser\Parser(
		$c['AutoloadPost\Model\Service\HtmlParser\HtmlParserInterface']
	);
};

$container['AutoloadPost\Model\File\UseCase\Parse\Handler'] = function ($c) {
	return new \AutoloadPost\Model\File\UseCase\Parse\Handler(
		$c['AutoloadPost\Model\File\Entity\FileRepository'],
		$c['AutoloadPost\Model\File\Service\Parser\Parser']
	);
};

$container[ParseAction::class] = function ($c) {
	return new ParseAction(
		$c['AutoloadPost\Model\File\UseCase\Parse\Handler']
	);
};

//--------------------------------------------

$container['AutoloadPost\Model\File\UseCase\Lists\Handler'] = function ($c) {
    return new \AutoloadPost\Model\File\UseCase\Lists\Handler(
        $c['AutoloadPost\Model\File\Entity\FileRepository'],
        $c['AutoloadPost\Model\File\Service\Parser\Parser']
    );
};

$container[ListAction::class] = function ($c) {
    return new ListAction(
        $c['AutoloadPost\Model\File\UseCase\Lists\Handler']
    );
};