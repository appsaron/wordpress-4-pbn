<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'example' );

/** MySQL database username */
define( 'DB_USER', 'admin_usr' );

/** MySQL database password */
define( 'DB_PASSWORD', 'Tsy2n9~6' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'j?7c_:s=bl{}UslcOcW>#|;d<=%pqBuuh 5,d]c5gV P^P;BELD^t4rB5FNqc9ZW' );
define( 'SECURE_AUTH_KEY',  '|bH  v8.q:3+VHcB-mal?-N[~V~O9iOq3j9js(GE56lrBv:75@o~3c*(MPT1NC5-' );
define( 'LOGGED_IN_KEY',    '~zWlJn+Kb6UetQtRAS6vM@Kn6^3GL.Y[I{$3S[l$I*igDBr3(m&Wi gYeu[,mI+-' );
define( 'NONCE_KEY',        '^Zx<:#bHe;,LaL2+ynwB1?mGcR@D)qV`chk[5t=maPLl-mJm;qb$zm>%W^fcx{0G' );
define( 'AUTH_SALT',        'h86[,RaAD0HBI`a.h>o5ZLj5R.Wfx4S9R<#V$.cuF0R8TWngQ }a6,43! <.!i:l' );
define( 'SECURE_AUTH_SALT', 'xFlr-d$*`df4FDMOnY_NI68~BMIJjUz6o@j6{><)Ru`F5!x9?Gg<Z`V9^eEuy*=I' );
define( 'LOGGED_IN_SALT',   '!k)S-Q/iHECnn#bmk&`b^MW%lSx]D}mU7yfK!=6E-@Jup97_,:$<hJzuJr-9,4)}' );
define( 'NONCE_SALT',       'ZPj)T#LM35l3B-Fh00tVE }S-zJ_5=Aiha2{,|t}P=[T2YCGN^aCOlW}J00Z6;`q' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
